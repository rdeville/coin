# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import json

from django.db.models import Q, Count
from django.views.generic.base import RedirectView
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseServerError
# from django.views.decorators.cache import cache_control

from coin.offers.models import Offer, OfferSubscription

class ConfigurationRedirectView(RedirectView):
    """Redirects to the appropriate view for the configuration backend of the
    specified subscription."""

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        subscription = get_object_or_404(OfferSubscription, pk=self.kwargs['id'],
                                         member=self.request.user)
        return reverse(subscription.configuration.url_namespace + ':' + subscription.configuration.backend_name,
                       args=[subscription.configuration.pk])


# @cache_control(max_age=7200)
def subscription_count_json(request):
    output = []

    # Get date form url, or set default
    date = request.GET.get('date', datetime.date.today())

    # Validate date type
    if not isinstance(date, datetime.date):
        try:
            datetime.datetime.strptime(date, '%Y-%m-%d')
        except ValueError, TypeError:
            return HttpResponseServerError("Incorrect date format, should be YYYY-MM-DD")

    # Get current offer subscription
    offersubscriptions = list(OfferSubscription.objects.running(date).offer_summary())
    for offersub in offersubscriptions:
        output.append({
            'reference' : offersub['offer__reference'],
            'name' : offersub['offer__name'],
            'subscriptions_count' : offersub['num_subscriptions']
        })

    # Return JSON
    return JsonResponse(output, safe=False)