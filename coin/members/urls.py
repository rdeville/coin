# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url
from coin.members import forms
from coin.members import views
from django.views.generic.base import TemplateView
from . import registration_views as views_r
from coin import settings

from registration.signals import user_activated
from django.contrib.auth import login

def login_on_activation(sender, user, request, **kwargs):
    """Logs in the user after activation"""
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    login(request, user)

# Registers the function with the django-registration user_activated signal
user_activated.connect(login_on_activation)

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^login/$', 'django.contrib.auth.views.login',
        {'template_name': 'members/registration/login.html',
         'extra_context': {'settings': settings} },
        name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login',
        name='logout'),

    url(r'^password_change/$', 'django.contrib.auth.views.password_change',
        {'post_change_redirect': 'members:password_change_done',
         'template_name': 'members/registration/password_change_form.html'},
        name='password_change'),
    url(r'^password_change_done/$', 'django.contrib.auth.views.password_change_done',
        {'template_name': 'members/registration/password_change_done.html'},
        name='password_change_done'),

    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset',
        {'post_reset_redirect': 'members:password_reset_done',
         'template_name': 'members/registration/password_reset_form.html',
         'email_template_name': 'members/registration/password_reset_email.html',
         'subject_template_name': 'members/registration/password_reset_subject.txt'},
        name='password_reset'),
    url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done',
        {'template_name': 'members/registration/password_reset_done.html',
         'current_app': 'members'},
        name='password_reset_done'),
    url(r'^password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect': 'members:password_reset_complete',
         'template_name': 'members/registration/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(r'^password_reset/complete/$', 'django.contrib.auth.views.password_reset_complete',
        {'template_name': 'members/registration/password_reset_complete.html'},
        name='password_reset_complete'),


    url(r'^activate/complete/$', views.activation_completed,
        name='registration_activation_complete'),
    # The activation key can make use of any character from the
    # URL-safe base64 alphabet, plus the colon as a separator.
    url(r'^activate/(?P<activation_key>[-:\w]+)/$',
        views_r.MemberActivationView.as_view(),
        name='registration_activate'),
    url(r'^register/$',
        views_r.MemberRegistrationView.as_view(
            form_class=forms.MemberRegistrationForm,
            template_name='members/registration/registration_form.html'
        ),
        name='registration_register'),
    url(r'^register/complete/$',
        TemplateView.as_view(
            template_name='members/registration/registration_complete.html'
        ),
        name='registration_complete'),
    url(r'^register/closed/$',
        TemplateView.as_view(
            template_name='members/registration/registration_closed.html'
        ),
        name='registration_disallowed'),
    #url(r'', include('registration.auth_urls')),




    url(r'^detail/$', views.detail,
        name='detail'),

    url(r'^subscriptions/', views.subscriptions, name='subscriptions'),
    # url(r'^subscription/(?P<id>\d+)', views.subscriptions, name = 'subscription'),

    url(r'^invoices/', views.invoices, name='invoices'),
    url(r'^contact/', views.contact, name='contact'),
)
