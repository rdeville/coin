# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from coin.resources.models import IPPool, IPSubnet

class IPPoolAdmin(admin.ModelAdmin):
    list_display = ('name', 'inet', 'default_subnetsize')
    ordering = ('inet',)


# TODO: don't display "Delegate reverse DNS" checkbox and Nameservers when
# creating/editing the object in the admin (since it is a purely
# user-specific parameter)
class IPSubnetAdmin(admin.ModelAdmin):
    list_display = ('inet', 'ip_pool', 'configuration')
    list_filter = ('ip_pool',)
    search_fields = ('inet',)
    ordering = ('inet',)


admin.site.register(IPPool, IPPoolAdmin)
admin.site.register(IPSubnet, IPSubnetAdmin)
