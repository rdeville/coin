# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.apps import AppConfig


class SimpleDSLConfig(AppConfig):
    name = 'simple_dsl'
    verbose_name = 'xDSl'
