from os.path import join, exists

import shutil
import tempfile

from django.conf import settings
from django.test import TestCase, override_settings

from coin.members.models import Member
from .models import MaillingList, MaillingListSubscription


@override_settings()
class SubscriptionTestCase(TestCase):
    def setUp(self):
        self.member = Member.objects.create(
            first_name=u"Toto",
            last_name=u"L'artichaut",
            username='toto',
            email='toto@example.com',
        )

        self.member2 = Member.objects.create(
            first_name=u"Lolo",
            last_name=u"Le Bigorneau",
            username='lolo',
            email='lolo@example.com',
        )

        self.tmpdir = tempfile.mkdtemp()
        settings.MAILLIST_SYNC_COMMAND = 'tee {}'.format(
            join(self.tmpdir, 'testlist-{short_name}'))
        self.ml_file = join(self.tmpdir, 'testlist-blabla')

        self.ml = MaillingList.objects.create(
            short_name='blabla',
            email='blabla@example.com',
            verbose_name='Blablateries',
            description='',
        )

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_subscription_sync(self):
        sub1 = MaillingListSubscription.objects.create(
            member=self.member,
            maillinglist=self.ml,
        )

        self.assertTrue(exists(self.ml_file))
        self.assertEqual(open(self.ml_file).read(), 'toto@example.com')

        MaillingListSubscription.objects.create(
            member=self.member2,
            maillinglist=self.ml,
        )

        self.assertEqual(
            open(self.ml_file).read(),
            'toto@example.com\nlolo@example.com',
        )

        sub1.delete()
        self.assertEqual(
            open(self.ml_file).read(),
            'lolo@example.com',
        )

    def test_email_change_update_subscriptions(self):
        MaillingListSubscription.objects.create(
            member=self.member,
            maillinglist=self.ml,
        )

        # then, change member email
        self.member.email = 'tata@example.com'
        self.member.save()
        self.assertEqual(open(self.ml_file).read(), 'tata@example.com')
