# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
import coin.apps


class MailListsConfig(AppConfig, coin.apps.AppURLs):
    name = 'maillists'
    verbose_name = "Listes mail"
    exported_urlpatterns = [('maillists', 'maillists.urls')]
