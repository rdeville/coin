FROM python:2
ENV PYTHONUNBUFFERED 1
RUN mkdir /src
WORKDIR /src
COPY requirements.txt /src/
# Copying code

RUN apt update 
RUN apt install -y \
        python-dev \
        python-pip \
        libldap2-dev \
        libpq-dev \
        libsasl2-dev \
        libjpeg-dev \
        libxml2-dev \
        libxslt1-dev \
        libffi-dev \
        python-cairo \
        libpango1.0-0 
RUN pip install -r requirements.txt
# RUN apt install curl

