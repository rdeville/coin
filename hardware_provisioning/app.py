# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.apps import AppConfig
import coin.apps


class HardwareProvisioningConfig(AppConfig, coin.apps.AppURLs):
    name = 'hardware_provisioning'
    verbose_name = 'Prêt de matériel'
    exported_urlpatterns = [('hardware_provisioning', 'hardware_provisioning.urls')]
