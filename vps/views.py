# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404
from django.views.generic.edit import UpdateView
from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from coin.members.models import Member

from .models import VPSConfiguration


class VPSView(SuccessMessageMixin, UpdateView):
    model = VPSConfiguration
    fields = ['ipv4_endpoint', 'ipv6_endpoint', 'comment']
    success_message = "Configuration enregistrée avec succès !"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(VPSView, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        if settings.MEMBER_CAN_EDIT_VPS_CONF:
            return super(VPSView, self).get_form(form_class)
        return None

    def get_object(self):
        if self.request.user.is_superuser:
            return get_object_or_404(VPSConfiguration, pk=self.kwargs.get("pk"))
        # For normal users, ensure the VPS belongs to them.
        return get_object_or_404(VPSConfiguration, pk=self.kwargs.get("pk"),
                                 offersubscription__member=self.request.user)

