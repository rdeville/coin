# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
import coin.apps

from . import urls


class VPSConfig(AppConfig, coin.apps.AppURLs):
    name = 'vps'
    verbose_name = "Gestion d'accès VPS"

    exported_urlpatterns = [('vps', urls.urlpatterns)]
