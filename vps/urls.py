# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import VPSView

urlpatterns = patterns(
    '',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', VPSView.as_view(template_name="vps/vps.html"), name="details"),
)
