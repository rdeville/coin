# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin

from coin.configuration.admin import ConfigurationAdminFormMixin
from coin.utils import delete_selected

from .models import VPSConfiguration, FingerPrint, Console


class ConsoleInline(admin.TabularInline):
    model = Console
    extra = 0


class FingerPrintInline(admin.TabularInline):
    model = FingerPrint
    extra = 0


class VPSConfigurationInline(admin.StackedInline):
    model = VPSConfiguration
    # fk_name = 'offersubscription'
    readonly_fields = ['configuration_ptr']


class VPSConfigurationAdmin(ConfigurationAdminFormMixin, PolymorphicChildModelAdmin):
    base_model = VPSConfiguration
    list_display = ('offersubscription', 'activated',
                    'ipv4_endpoint', 'ipv6_endpoint', 'comment')
    list_filter = ('activated',)
    search_fields = ('comment',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    actions = (delete_selected, "generate_endpoints", "generate_endpoints_v4",
               "generate_endpoints_v6", "activate", "deactivate")
    inline = VPSConfigurationInline

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return []
        else:
            return []

    def set_activation(self, request, queryset, value):
        count = 0
        # We must update each object individually, because we want to run
        # the save() method to update the backend.
        for vps in queryset:
            if vps.activated != value:
                vps.activated = value
                vps.full_clean()
                vps.save()
                count += 1
        action = "activated" if value else "deactivated"
        msg = "{} VPS subscription(s) {}.".format(count, action)
        self.message_user(request, msg)

    def activate(self, request, queryset):
        self.set_activation(request, queryset, True)
    activate.short_description = "Activate selected VPSs"

    def deactivate(self, request, queryset):
        self.set_activation(request, queryset, False)
    deactivate.short_description = "Deactivate selected VPSs"

    def generate_endpoints_generic(self, request, queryset, v4=True, v6=True):
        count = 0
        for vps in queryset:
            if vps.generate_endpoints(v4, v6):
                vps.full_clean()
                vps.save()
                count += 1
        msg = "{} VPS subscription(s) updated.".format(count)
        self.message_user(request, msg)

    def generate_endpoints(self, request, queryset):
        self.generate_endpoints_generic(request, queryset)
    generate_endpoints.short_description = "Generate IPv4 and IPv6 endpoints"

    def generate_endpoints_v4(self, request, queryset):
        self.generate_endpoints_generic(request, queryset, v6=False)
    generate_endpoints_v4.short_description = "Generate IPv4 endpoints"

    def generate_endpoints_v6(self, request, queryset):
        self.generate_endpoints_generic(request, queryset, v4=False)
    generate_endpoints_v6.short_description = "Generate IPv6 endpoints"

VPSConfigurationAdmin.inlines = VPSConfigurationAdmin.inlines + (FingerPrintInline, ConsoleInline )
admin.site.register(VPSConfiguration, VPSConfigurationAdmin)
